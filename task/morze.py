def code_morze(value):
    '''
    please add your solution here or call your solution implemented in different function from here
    then change return value from 'False' to value that will be returned by your solution
    '''
    morse_code_dict = {
        'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.', 'G': '--.', 'H': '....', 'I': '..',
        'J': '.---', 'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.', 'O': '---', 'P': '.--.', 'Q': '--.-', 'R': '.-.',
        'S': '...', 'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 'Y': '-.--', 'Z': '--..',
        '1': '.----', '2': '..---', '3': '...--', '4': '....-', '5': '.....', '6': '-....', '7': '--...', '8': '---..',
        '9': '----.', '0': '-----', ',': '--..--', '.': '.-.-.-', '?': '..--..', '/': '-..-.', '-': '-....-',
        '(': '-.--.', ')': '-.--.-'
    }

    morse_result = []
    for char in value.upper():
        if char == ' ':
            continue
        if char in morse_code_dict:
            morse_result.append(morse_code_dict[char])

    morse_message = ' '.join(morse_result)

    return morse_message

input_value = "Data Science - 2023"
encoded_value = code_morze(input_value)
print(encoded_value)


